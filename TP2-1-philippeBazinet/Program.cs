﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_1_philippeBazinet
{
    internal class Program
    {
        /// <summary>
        /// TITRE:          GROSSES COUPURES
        /// DESCRIPTION:    PROGRAMME QUI SAISIT UNE SOMME DE DOLLARS ENTIÈRE ET QUI AFFICHE UN RAPPORT DÉMONTRANT LE NOMBRE MINIMAL DE COUPURES DE 100, 50, 20, 10, 5, 2 ET 1$ REQUISES POUR ÉGALER LA SOMME ENTRÉE PAR L'UTILISATEUR.
        /// FAIT PAR:       PHILIPPE BAZINET
        /// FAIT LE:        9 FÉVRIER 2022
        /// RÉSIVÉ LE:      -
        /// NOTE:           PROGRAMME CODÉ AVEC UNE LOGIQUE BASÉE SUR DES MODULOS ET DES DÉCRÉMENTATIONS SUCCESSIVES
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //variables                                             //details
            ushort userAmount;                                      //user input will be redirected to this variable, 0 < userAmount < 65535$. We will increment it too.
            string str_userAmount;                                  //to verify if we can ushort.Parse this number and avoir error
            bool correctAmount;                                     //if str_userAmount can be converted to ushort
            string doItAgain;                                       //to retype a new number
            ushort cut100, cut50, cut20, cut10, cut5, cut2, cut1;   //we push the number of cut to these variables

            //START
            do
            {
                /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                //1 - prompting the user
                do
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("Enter an integer amount of money between 0.00$ and 65535.00$: ");
                    str_userAmount = Console.ReadLine();
                    correctAmount = ushort.TryParse(str_userAmount, out userAmount);
                    Console.ForegroundColor = ConsoleColor.White;
                    if (!correctAmount)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("You must enter an integer value between 0.00$ and 65535.00$...");
                    }
                    /*
                    else
                    {
                        userAmount = ushort.Parse(str_userAmount);      //if the value is good, we assign the user input to userAmount
                    }
                    */
                } while (!correctAmount);
                Console.ForegroundColor = ConsoleColor.White;

                /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                //2 - calculating with modulos and remainders - decrementing userAmount everytime
                //each money cut cannot have more cuts than its own value! 1 coin of 1.00$ maximum, etc...
                cut100 = (ushort)((userAmount - (userAmount % 100)) / 100);
                userAmount = (ushort)(userAmount - cut100 * 100);           //we decrement userAmount, it now shows the remaining money to evaluate

                cut50 = (ushort)((userAmount - (userAmount % 50)) / 50);
                userAmount = (ushort)(userAmount - cut50 * 50);             //decrementing again and again...

                cut20 = (ushort)((userAmount - (userAmount % 20)) / 20);
                userAmount = (ushort)(userAmount - cut20 * 20);

                cut10 = (ushort)((userAmount - (userAmount % 10)) / 10);
                userAmount = (ushort)(userAmount - cut10 * 10);

                cut5 = (ushort)((userAmount - (userAmount % 5)) / 5);
                userAmount = (ushort)(userAmount - cut5 * 5);

                cut2 = (ushort)((userAmount - (userAmount % 2)) / 2);
                userAmount = (ushort)(userAmount - cut2 * 2);

                cut1 = userAmount;                                          //more than 1 coin is impossible!

                /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                //results returned to user
                Console.WriteLine("To obtain the smallest amount of bills and coins, you would receive:");
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                if (cut100 != 0)    
                { Console.WriteLine("* {0} bill{1} of 100.00$", cut100, (cut100 > 1) ? "s" : ""); } //Dealing with plural and singular with TERNARY
                if (cut50 != 0)     
                { Console.WriteLine("* {0} bill{1} of 50.00$", cut50, (cut50 > 1) ? "s" : ""); }
                if (cut20 != 0)     
                { Console.WriteLine("* {0} bill{1} of 20.00$", cut20, (cut20 > 1) ? "s" : ""); }
                if (cut10 != 0)     
                { Console.WriteLine("* {0} bill{1} of 10.00$", cut10, (cut10 > 1) ? "s" : ""); }
                if (cut5 != 0)      
                { Console.WriteLine("* {0} bill{1} of 5.00$", cut5, (cut5 > 1) ? "s" : ""); }
                if (cut2 != 0)     
                { Console.WriteLine("* {0} coin{1} of 2.00$", cut2, (cut2 > 1) ? "s" : ""); }
                if (cut1 != 0)    
                { Console.WriteLine("* {0} coin of 1.00$", cut1); }

                /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                //do it again?
                Console.ForegroundColor= ConsoleColor.Cyan; 
                Console.Write("Do you want to evaluate a new number? Type yes if you want, or type any other key and/or press ENTER to exit. ");
                doItAgain = Console.ReadLine().ToLower();
                Console.Clear();

            } while (doItAgain == "yes");
        }
    }
}
//END
